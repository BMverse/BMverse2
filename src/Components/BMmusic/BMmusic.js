import React from "react";
import './BMmusic.css';
import Header from "../General_Components/Header";
import MainText from "../General_Components/MainText/MainText";
// import Glowbg from "../General_Components/Glowbg/Glowbg";
import Footer from "../General_Components/Footer/Footer";
import SideStrip from "../General_Components/SideStrip/SideStrip";
import Menu from "../General_Components/Menu/Menu";
import music1 from './Images/music1.png';
import music2 from './Images/music2.png';
import music3 from './Images/music3.png';
import github from './Images/GitHub.png'
const Music = ()=>{

    return(
        <React.Fragment>
            <div className="BMmusic">
            <section  className="main_section">   
            <Header code="1"></Header>
            <Menu></Menu>
            <SideStrip></SideStrip>
            {/* <MainText></MainText> */}
            <h1 className="MainText"> BM MUSIC</h1>
            <p className="identity">IDENTITY</p>

            <h3 className="heading_h3">
            <span className="music_span">BM M</span>USIC IS ACTUALLY A PLATFORM FOR ROCORDING ARTISTIC WORKS IN THE MUSIC
            INDUSTORY. <span  className="music_span">W</span>ITH THE PLATFORM, EVERY ARTIST IN THE MUSIC INDUSTRY WILL BE SURE 
            THAT HE/SHE WILL HAVE ALL THE MORAL AND PROPERTY RIGHTS OF HIS/HER
            WORKS FOREVER. <span  className="music_span">M</span>USIC PLAYS AN ESSENTIAL ROLE IN THE PROGRESS OF THE <span  className="music_span">BMV</span>ERSE PROJECT,
            BECAUSE THE INDUSTRY CARDS PURCHASED TO ENTER THE <span  className="music_span">BMG</span>ROUP <span>M</span>ETAVERSE WILL BE.
            </h3>

            <div className="github">
                <img src={github}></img>
            </div>
          
          <h1 className="heading_h1">
          <span  className="music_span">I</span>N FACT, VERIFIED BY THE WAVE PRODUCED BY EACH TRACK
          </h1>

          <h2 className="heading_h2">
            THE WORD "<span  className="music_span">I</span>NDUSTRY" REFERS TO THE IDENTITY OF THE CHARACTERS. <span  className="music_span">L</span>ET'S SAY IT MORE CLEARLY, THE OWNER OF THE 
            IDENTITY OF ANY CHARACTER CAN HAVE THE ABILITY TO TRAVEL IN <span  className="music_span">M</span>ETAVERSE OF <span>BM M</span>EGA AND <span>BM G</span>ALAXY
          </h2>

          <div className="img_div">
            <img src={music1}></img>
            <img src={music2}></img>
            <img src={music3}></img>
          </div>

            </section>
            <Footer></Footer>
            

            </div>
            
        
        </React.Fragment>
    )
};

export default Music;