import React from "react";
import './SideStrip.css';

import linked from './images/linked.png';
import twitter from './images/twitter.png';
import youTube from './images/youTube.png';
import ballon from './images/ballon.png';



const SideStrip =()=>{
    return(
        <section className="SideStrip">
            <div className="sideslide_div">
                <h1>Follow Us On:</h1>
                <a href="https://twitter.com/bmverseio"><img src={twitter}></img></a>
                <a href="https://www.linkedin.com/company/bmverse/"><img src={linked}></img></a>
                <a href="https://www.youtube.com/channel/UC9_6umU_VkYZ2v6Rvl6BLMQ/"><img src={youTube}></img></a>
                <a href="https://medium.com/@bmverseio"><img src={ballon}></img></a>
            </div>
            
         </section>
    );


}
export default SideStrip;
