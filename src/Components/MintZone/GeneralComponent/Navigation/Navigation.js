import React from "react";
import './Navigation.css';

import Logo from './Images/LogoGod.png'
import smallLogo from './Images/logo.png'

const Navigation =()=>{
    return(
        <React.Fragment>
            <div className="Navigation">
                <div className="Nav_image">
                 <img src={Logo}></img>
                </div>

                <div className="Nav_text">
                    <img src={smallLogo}></img>
                    <h1>
                    BMVerse.io
                    </h1>
                </div>
                <div className="Nav_button">
                    <a href="#">Connect wallet</a>
                </div>
            </div>

             

        </React.Fragment>
    )
}

export default Navigation;