import React from "react";
import Header from "../GeneralComponent/Header/Header";
import Navigation from "../GeneralComponent/Navigation/Navigation";
import './MintPage.css';
import Crousel from "./CrouselMint";


// imges import-------------------------
import roundNft from './Images/roundNft.png';
import girlNft from './Images/girlNft.png';
import girlNft2 from './Images/girlNft2.png';
import girlNft3 from './Images/girlNft3.png';
import girlNft4 from './Images/girlNft4.png';
import girlNft5 from './Images/girlNft5.png';
import girlNft6 from './Images/girlNft6.png';
import mintButton from './Images/mintButton.png';
import editButton from './Images/editButton.png';
import mintLineButton from './Images/mintLineButton.png';
import one from './Images/1.png';
import two from './Images/2.png';
import three from './Images/3.png';
import four from './Images/4.png';
import five from './Images/5.png';
import six from './Images/6.png';

import blue from './Images/blue.png';
import brown from './Images/brown.png';
import green from './Images/green.png';
import pink from './Images/pink.png';
import purple from './Images/purple.png';
import parrot from './Images/parrot.png';
import dragon from './Images/dragon.png';
import circle from './Images/circle.png';
import bm from './Images/bm.png';
import card3 from './Images/card3.png';
import card1 from './Images/card1.png';
import gade from './Images/gade.png';

const Mintpage =()=>{
    return(
        <section className="mint_page">

            <Navigation></Navigation>
            <Header></Header>
            <div className="mint_main_heading">
                <h1>ANTverse series from BM Mega</h1>
                <h3>Mint Zone</h3>
            </div>

            <div className="mint_box_1">
                <h1 className="after_line">EPIC Assets</h1>
                <div className="assect_box">
                    <div className="assect_box_1">
                        <img src={roundNft}></img>
                        <a href="#"><img src={mintButton}></img></a>

                    </div>
                    <div className="assect_box_2">
                        <div className="Crousel">
                    <Crousel
                     imgs={[
                        girlNft,
                        girlNft2,
                        girlNft3,
                        girlNft4,
                        girlNft5,
                        girlNft6
                        
                            ]}
      />
      </div>

                        {/* <img src={girlNft}></img> */}
                        <div className="assect_box_2_text">
                            <h2>Rarity 1/1</h2>
                            <p>Every person who buys the character does not necessarily
own all the sub assets, and only by purchasing the character,
3 Dynamic Sub-assets will be airdrop for her. </p>
                        </div>

                    </div>


                </div>


            </div>
                   
                   <div className="mint_box_2">
                    <h1>Magical Ability</h1>
                    <div className="mint_box_2_div_1">
                        <p>1/1</p>
                        <img src={dragon}></img>
                        <h2>Miracle</h2>
                    </div>

                    <div className="mint_box_2_div_2">
                        <div className="sub_div_1">
                            <h3>Potion Cards</h3>
                            <img src={circle}></img>
                        </div>
                        <a href="#"><img src={mintLineButton}>
                           </img>
                        </a>
                        <div className="sub_div_2">
                            <img src={bm}></img>
                            <h3>Potion Cards</h3>
                        </div>
                        
                    </div>
                     <div className="responsive_mint">
                     <a href="#"><img src={mintButton}>
                           </img>
                        </a>
                     </div>
                   


                    <div className="mint_box_2_div_3">
                        <div className="sub_div_3">
                            <img src={card3}></img>
                            <p>1/3</p>
                        </div>
                       
                        <div className="sub_div_3">
                            <img src={card1}></img>
                            <p>1/1</p>
                        </div>
                    </div>
                    <div className="gade_img">
                         <img src={gade}></img>
                    </div>



                   </div>


            <div className="mint_box_3">
                <div className="mint_box_3_text">
                    <h1>IDENTITY</h1>
                    <h3>BMMusic</h3>
                </div>
                <div className="mint_box_3_images">
                    <img src={one}></img>
                    <img src={two}></img>
                    <img src={three}></img>
                    <img src={four}></img>
                    <img src={five}></img>
                    <img src={six}></img>

                </div>
                <div className="mint_box_3_num">
                    1/1
                </div>
            </div>


            <div className="mint_box_4">
                <h3 className="mint_box_4_text_1">
                the word “Identity” refers to the identity of the characters. Let’s say it more clearly, the owner<br></br>
                of the identity of any character can have the ability to travel in Metaverse of AntVerse and BM Galaxy.

                </h3>
                <h2 className="mint_box_4_text_2">
                Metaverse will be verified by the music (in fact by the wave produced by each track) existed in the identity card.
                </h2>
                <a href="#"><img src={mintButton}></img></a>

                <h1>Dynamic Assets 1/99</h1>
            </div>


            <div className="mint_box_5">
                <div className="mint_box_5_images">
                    <img src={blue}></img>
                    <img src={brown}></img>
                    <img src={green}></img>
                    <img src={pink}></img>
                    <img src={purple}></img>
                    <img src={parrot}></img>
                </div>
            </div>


            <h2 className="mint_last_text">10% of all purchases and sales of sub assets go to the owner of the epic character
for allowing them access to the read only version.</h2>

                <div className="mint_box_6">
                    <a href="#"><img src={mintButton}></img></a>
                    <a href="#"><img src={editButton}></img></a>
                </div>

        </section>
    )
}

export default Mintpage;




