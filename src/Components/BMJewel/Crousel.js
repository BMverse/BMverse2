import { useState, useEffect } from "react";
import './Crouseljwel.css'

const Crousel = ({ imgs }) => {
  const [index, setIndex] = useState(0)

  useEffect(() => {
    setIndex(0)
  }, [])

  const next = () => {
    if (index === imgs.length - 1) {
      setIndex(0)
    } else {
      setIndex(index + 1)
    }
  }
  const prev = () => {
    if (index === 0) {
      setIndex(imgs.length - 1)
    } else {
      setIndex(index - 1)
    }
  }


  return (
    <div className="slideshowjewl">
      <img className="mainImgjewl" src={imgs[index]} />
      <div className="actionsjewl">
        <button onClick={prev}>&#x2190;</button>
        <button onClick={next}>&#x2192;</button>
      </div>
    </div>
  )
}

export default Crousel;